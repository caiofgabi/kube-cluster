#!/bin/bash

# setup.sh

# Este script foi criado para instalar e configurar o Ansible na máquina que será utilizada para acessar as máquinas do desafio.

echo "*** Atualizando o cache do repositório ***"
sudo apt update

echo "*** Instalando dependências e o Ansible ***"
sudo apt install -y software-properties-common ansible

echo "*** Criando chaves SSH do Ansible ***"
sudo ssh-keygen -t rsa

echo "*** Configurando permissões de chave SSH para acessar as máquinas do Cluster Kubernetes ***"
sudo chmod 600 ./.ssh/sogo-rec.pem

