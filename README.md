# Introdução

Este repositório foi criado para guardar a solução do desafio devops proposto pelo Alfa Group.

O desafio consiste em instalar e configurar um cluster kubernetes em dois servidores e fazer com que uma aplicação de demonstração e outra em Golang seja hospedada no worker do Cluster Kubernetes e seja disponibilizada na internet.

## Relação de Arquivos e Diretórios

Nome                  | Descrição
:-------------------: | ------
setup.sh              | Shell script de configuração do controlador ansible
clusterNodes          | Inventário ansible contendo a configuração de conexão com as VMs do cluster kubernetes
dependencies.yml      | Playbook ansible para instalar as dependências do cluster kubernentes
masters.yml           | Playbook ansible para configuração do Master node do cluster
workers.yml           | Playbook ansible para configuração do Worker node do cluster
kube-deployments      | Diretório contendo playbooks kubernetes para o deployment da aplicação nginx (nginx-deploy.yml e nginx-service.yml)
nginx-app-deploy.yml  | Playbook ansible para fazer o deployment da aplicação nginx no Master node
desafio-devops        | Diretório contendo os arquivos necessários para build e deployment da aplicação Golang
golang-app-deploy.yml | Playbook ansible para fazer o deployment da aplicação Golang no Master node

# Resolução

## Instalação do Cluster Kubernetes

Neste desafio, o Ansible será utilizado para gerenciar a configuração e a implantação do cluter Kubernetes.

Inicialmente, para configurar o controlador Ansible, foi criado o arquivo **setup.sh**, que é um shell script com a função de instalar o Ansible no controlador e configurar as permissões da chave SSH disponibilizada para o desafio.

Posteriormente, foi criado o arquivo de inventário Ansible **clusterNodes**. Neste arquivo são criados dois grupos: masters e workers. O grupo masters contém o servidor (ou possíveis servidores) que servirá de master no cluster Kubernetes, enquanto o grupo workers contém o servidor (ou possíveis servidores) que servicá de worker no cluster Kubernetes. Além disso, também é feita a configuração de conexão SSH com cada um desses servidores para que o Ansible consiga executar as tarefas corretamente.

Para instalar as dependências do cluster Kubernetes foi criado um Playbook Ansible chamado **dependencies.yml**. Depois de rodar o playbook de dependências, o Docker, o Kubeadm e Kubelet devem estar instalados em todos os nodes e o Kubectl deve estar instalado no Master. Além disso, foi feito um ajuste na configuração do docker para corrigir um erro de comunicação entre o Docker e o Kubernetes.

Após a instalação das dependências, é feita a inicialização do cluster Kubernetes no node Master utilizando o playbook **masters.yml**. A inicialização do cluster é feita com a rede de pods sendo configurada para a faixa de IPs 10.244.0.0/16, que é requisito para o funcionamento do Flannel, que também é aplicado ao cluster com esse playbook.

Para finalizar a instalação do Cluster, é feita a configuração do node Worker para que ele seja inserido no Cluster utilizando o playbook **workers.yml**. Neste playbook é gerado um token no node Master para configurar comando de entrada do node worker no cluster e esse comando é aplicado.

## Deployment de Aplicação Nginx

O desafio requer que uma aplicação nginx, contendo 3 réplicas de pods, seja colocada no cluster através de um deployment e que esses pods sejam rodados apenas no worker node. Para realizar isso, foi criado um arquivo **nginx-deploy.yml**, no diretório **kube-deployments** no qual a aplicação nginx é configurada para ter as 3 réplicas rodando e utiliza um nodeSelector para indicar qual o node que a aplicação deve rodar.

Além disso, o desafio também requer que essa aplicação nginx fique exposta no IP externo do worker node na porta 30001. Para tanto, foi criado um arquivo **nginx-service.yml**, no diretório **kube-deployments**, que cria um service kubernetes do tipo NodePort e expõe a aplicação na porta 30001.

Para aplicar esses playbooks no cluster kubernetes foi criado um playbook ansible **nginx-app-deploy.yml**, que configura as labels do worker node para que o nodeSelector do deployment funcione corretamente, copia os playbooks kubernetes do diretório local para o master node e aplica os playbooks no cluster kubernetes.

## Deployment de Aplicação Golang

Para o deployment da aplicação Golang foi feita a clonagem do repositório https://gitlab.com/sogo_tip/desafio-devops, que contém todos os arquivos necessários para o deployment dessa aplicação. Depois de clonar o repositório, foi feita a configuração do arquivo de build da imagem docker para que a imagem criada pelo build fosse enviada para o Gitlab Registry https://gitlab.com/caiofgabi/kube-cluster/container_registry. Além disso, o arquivo de deployment da aplicação foi modificado para fazer o pull da imagem desse repositório.

Para fazer o deployment foi criado um playbook ansible **golang-app-deploy.yml**, que desabilita a exposição da porta 30001 para a aplicação nginx do passo anterior, copia os arquivos da aplicação Golang para o Master node, faz o build da imagem docker e aplica o deployment e o service da aplicação golang no cluster.

De acordo com o pedido no desafio, é possível verificar o status da aplicação no endereço:

http://3.101.113.22:30001/_status/healthz

# Considerações Finais

Foi bem interessante trabalhar nesse desafio. Aproveitei a oportunidade para testar meus conhecimentos com uma ferramenta além do que foi solicitado, que foi o ansible e isso acabou dando uma dimensão a mais de dificuldade no início, mas também ajudou a resolver os problemas que foram aparecendo à medida que fui me acostumando com o que estava fazendo. Espero ter conseguido entender o desafio de acordo com o que ele foi proposto e ter feito tudo dentro do esperado.

Obrigado.

@caiofgabi